
import React, { useState } from "react";
import axios from "axios";
// import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
function Register() {
// use State

  const history = useHistory();
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [nomor, setNomor] = useState();
  const [foto, setFoto] = useState("");

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
// end UseState
// method: register
  const register = async (e) => {
      e.preventDefault();
      const fromData = new FormData()
 
      fromData.append("nama", nama)
      fromData.append("foto", foto)
      fromData.append("nama", nama)
      fromData.append("alamat", alamat)
      fromData.append("nomor", nomor)
      fromData.append("email", email)
      fromData.append("password", password)
      fromData.append("role", "USER")
      try {
      const response =  await axios.post("http://localhost:3010/user/sign-up",fromData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });
        if (response.status == 200) {
          alert("register succes");
          setTimeout(() => {
            history.push("/");
          }, 1250);
        }
      } catch (err) {
        console.log(err);
      }
    };
    // end register
    // isi register
  return (
    <div>
 

<div class="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
  <div class="mx-auto max-w-lg">
 

    <form  onSubmit={register} action="" class="mt-6 mb-0 space-y-4 rounded-lg p-8 shadow-2xl">
    <h1 class="text-center text-2xl font-bold text-indigo-600 sm:text-3xl">
   Register
    </h1>

    <p class="mx-auto mt-4 max-w-md text-center text-gray-500">
    Buat Akun Anda Untuk  Mengakses fitur Aplikasi kami
    </p>
{/* email */}
      <div>
        <label for="email" class="text-sm font-medium">Email</label>

        <div class="relative mt-1">
          <input
            type="text"
            id="email"
            class="w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
            placeholder="Enter email"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
          />

          <span class="absolute inset-y-0 right-4 inline-flex items-center">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-5 w-5 text-gray-400"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207"
              />
            </svg>
          </span>
        </div>
      </div>
      {/* end email */}
{/* password */}
      <div>
        <label for="password" class="text-sm font-medium">Password</label>

        <div class="relative mt-1">
          <input
            type="password"
            id="password"
            class="w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
            placeholder="Enter password"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
          />

          <span class="absolute inset-y-0 right-4 inline-flex items-center">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-5 w-5 text-gray-400"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
              />
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
              />
            </svg>
          </span>
        </div>
      </div>
{/* end pasword */}

      <button
        type="submit"
        class="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white hover:scale-105"
      >
        Sign in
      </button>

      <p class="text-center text-sm text-gray-500">
      Jika memiliki akun
        <a class="underline" href="/">Login</a>
      </p>
    </form>
  </div>
</div>

    </div>
  )
  // end register
}

export default Register