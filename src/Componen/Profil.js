import React, { useState, useEffect } from "react";
import axios from "axios";
import Navbar from "../Componen/NavigationBar";
import { BsXSquareFill } from "react-icons/bs";
import { AiFillMail } from "react-icons/ai";
import { TiLocation } from "react-icons/ti";
import { BsFillTelephoneFill, BsPencilSquare } from "react-icons/bs";
import Swal from "sweetalert2";
import { Modal, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
export default function Profil() {
  const [foto, setFoto] = useState("");
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [nomor, setNomor] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [email, setEmail] = useState("");
  const [show1, setShow1] = useState(false);
  const [show, setShow] = useState(false);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);
  const history = useHistory();
  const [user, setUser] = useState([]);
  const [list, setList] = useState([]);
  // Get Profil

  // const Profile = async () => {
  //   await axios
  //     .get("http://localhost:3010/profil/user" + localStorage.getItem("userId"))
  //     .then((res) => {
  //       const result = res.data.data
  //       setUser(res.data.data);
  //       setNama(res.data.data);
  //       setAlamat(res.data.data);
  //       setNomor(res.data.data);
  //       setFoto(res.data.data);
  //       console.log(result);
  //     })
  //     .catch((error) => {});
  // };

  const getUser = async () => {
    await axios
      .get("http://localhost:3010/profil/" + localStorage.getItem("userId"))
      .then((res) => {
        setUser(res.data.data);
        setNama(res.data.data);
        setAlamat(res.data.data);
        setNomor(res.data.data);
        setFoto(res.data.data);
        sessionStorage.setItem("profil", "Sudah");
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const add = async (e) => {
    e.preventDefault();
    const fromData = new FormData();

    fromData.append("nama", nama);
    fromData.append("foto", foto);
    fromData.append("deskripsi", deskripsi);
    fromData.append("alamat", alamat);
    fromData.append("nomor", nomor);

    fromData.append("userId", localStorage.getItem("userId"));

    try {
      const response = await axios.post(
        "http://localhost:3010/profil",
        fromData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      sessionStorage.setItem("profil", "Sudah");
      if (response.status == 200) {
        alert("register succes");
        setTimeout(() => {
          window.location.reload();
          history.push("/profil");
        }, 1250);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Add profile

  useEffect(() => {
    getUser();
  }, []);

  return (
    <div className="bg-gradient-to-r from-slate-300  to-cyan-500">
      <Navbar />
      

      {/* modal */}
      <Modal
        show={show1}
        onHide={handleClose1}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>List yang Mau Di lakukan</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={add}>
            <Form.Group className="mb-3" controlId="formBasictext">
              <Form.Label>Nama</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nama Anda"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
                required
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasictext">
              <Form.Label>deskripsi</Form.Label>
              <Form.Control
                type="text"
                placeholder="Info"
                value={deskripsi}
                onChange={(e) => setDeskripsi(e.target.value)}
                required
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicDate">
              <Form.Label>alamat</Form.Label>
              <Form.Control
                type="text"
                placeholder="Alamat"
                value={alamat}
                onChange={(e) => setAlamat(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicDate">
              <Form.Label>Nomor Hp</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nomor Hp"
                value={nomor}
                onChange={(e) => setNomor(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicDate">
              <Form.Label>Foto</Form.Label>
              <Form.Control
                type="file"
                placeholder="Profil"
                // value={akhir}
                onChange={(e) => setFoto(e.target.files[0])}
                required
              />
            </Form.Group>
            <div className="flex gap-2">
              {/* jika di close maka akan keluar */}
              <button onClick={handleClose1}>
                <a class="inline-block rounded bg-red-600 px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500">
                  Close
                </a>
              </button>
              <br />
              {/* jika di save maka akan menyimpan */}
              <button type="submit">
                <a class="inline-block rounded bg-green-600 px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500">
                  Tambahkan
                </a>
              </button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      {sessionStorage.getItem("profil") !== "Sudah" ? (
              <>
              <div class=" bg-gray-200 dark:bg-gray-900 flex flex-wrap items-center justify-center">
  <div class="container max-w-lg bg-white rounded dark:bg-gray-800 shadow-lg transform duration-200 easy-in-out m-12">
    <div class="h-2/4 sm:h-64 overflow-hidden">
      <img class="w-full rounded-t"
        src="https://tse2.mm.bing.net/th?id=OIP.58t72HiyqKUNI4D4xHj1gQHaHa&pid=Api&P=0"
        alt="Photo by aldi sigun on Unsplash" />
    </div>
    <div class="flex justify-start px-5 -mt-12 mb-5">
      <span clspanss="block relative h-32 w-32">
        <img alt="Photo by aldi sigun on Unsplash"
          src="https://tse3.mm.bing.net/th?id=OIP.K263CwH6jn1uTmm6Fwc2cAAAAA&pid=Api&P=0"
          class="mx-auto object-cover rounded-full h-24 w-24 bg-white p-1" />
      </span>
    </div>
    <div class="">
      <div class="px-7 mb-8">
        <h2 class="text-3xl font-bold text-green-800 dark:text-gray-300">Nama Kamu</h2>
     <br />
       <button onClick={handleShow1}>
       <a
  class="inline-block rounded bg-indigo-600 px-8 py-3 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500"
  
>
 Buat Profil
</a>
      </button>
  
      </div>
    </div>
  </div>
</div>

              </>
            ) : (
              <>
                {" "}
                <div class=" bg-gray-200 dark:bg-gray-900 flex flex-wrap items-center justify-center">
        <div class="container max-w-lg bg-white rounded dark:bg-gray-800 shadow-lg transform duration-200 easy-in-out m-12">
          <div class="h-2/4 sm:h-64 overflow-hidden">
            <img
              class="w-full rounded-t"
              src="https://dagodreampark.co.id/images/ke_2.jpg"
              alt="Photo by aldi sigun on Unsplash"
            />
          </div>
          <div class="flex justify-start px-5 -mt-12 mb-5">
            <span clspanss="block relative h-32 w-32">
              <img
                alt="Photo by aldi sigun on Unsplash"
                src={user.foto}
                class="mx-auto object-cover rounded-full h-24 w-24 bg-white p-1"
              />
            </span>
          </div>
          <div class="">
            <div class="px-7 mb-8">
              <h2 class="text-3xl font-bold text-green-800 dark:text-gray-300">
                {user.nama}
              </h2>

              <p class="mt-2 text-gray-600 dark:text-gray-300">
                {/* {deskripsi} */}
              </p>
              <br />

              <div className="flex gap-[35%]">
                <div>
                  <h2 className="flex gap-1">
                    <BsFillTelephoneFill />
                    {user.nomor}
                  </h2>

                  <h2 className="flex">
                    <TiLocation />
                    {user.alamat}
                  </h2>
                </div>
              </div>
              <br />
              <div className="flex">
             

              
                {" "}
                <button className="ml-[8%]">
                  <a href={"/editprofil/" + user.id}>
                    <span class="flex gap-1  ml-[4%] mt-[] rounded hover:bg-indigo-400 bg-indigo-600 px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500">
                      <BsPencilSquare />
                      Profil
                    </span>
                  </a>
                </button>
                <button className="ml-[4%]">
                  <span class="flex gap-1  ml-[4%] mt-[] rounded hover:bg-indigo-400 bg-indigo-600 px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500">
                    <BsPencilSquare />
                    Latar
                  </span>
                </button>
           
                
              </div>
            </div>
          </div>
        </div>
      </div>
              </>
            )}
      {/* modal */}
      
    </div>
  );
}
