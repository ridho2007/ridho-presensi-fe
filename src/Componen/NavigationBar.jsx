import React, { useState } from "react";
import { BsPersonSquare } from "react-icons/bs";
import { BiLogIn } from "react-icons/bi";
import { useHistory } from "react-router-dom";
import { FaRProject,FaUserAlt } from "react-icons/fa";
function NavigationBar() {
    // use state
    const [show, setShow] = useState(false);
    const history = useHistory();
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
// end  useState
    const logout = () => {
        localStorage.clear();
        sessionStorage.clear();
        history.push("/");
        window.location.reload();
      };
   
    return (
        <div className='bg-gradient-to-r from-sky-600  to-cyan-800'>
            <header aria-label="Site Header" class="border-b  shadow-2xl">
                <div
                    class="mx-auto flex h-16 w-[100%] items-center justify-between sm:px-6 lg:px-8"
                >
                    <div class="flex items-center gap-2">
                     <div className='text-6xl '> <FaRProject /></div>
                       
                        <a
                            href="/home"
                            class="mt-5 text-gray-300 block h-16   g:text-xs lg:font-bold lg:uppercase hover:text-white"
                        >
                            <h1> Project</h1>
                        </a>

                    </div>

                    <div class="flex  flex-1 items-center justify-end">
                        

                        <div class="ml-8  flex items-center lg:text-xs lg:font-bold lg:uppercase lg:tracking-wide lg:text-gray-200">
                            <div
                                class="flex items-center gap-4"
                            >
                            
                            <a href="/profil">
            <span className="text-2xl ">  <BsPersonSquare/></span></a>
                           {localStorage.getItem("role") !== "USER" ? (
              <></>
            ) : (
              <>
                {" "}
                <button  className="btn text-2xl text-white" onClick={logout}>
                  <BiLogIn/>
                </button>
              </>
            )}
       
          
     
                              

                            
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    )
}

export default NavigationBar