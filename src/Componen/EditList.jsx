
import { useHistory, useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";
import axios from "axios";
import Swal from "sweetalert2";
export default function EditList() {
  const history = useHistory();
  const [task, setTask] = useState("");
  const [mulai, setMulai] = useState();
  const [akhir, setAkhir] = useState();
  const [laporan, setLaporan] = useState("");
  const param = useParams();
  useEffect(() => {
    axios
      .get("http://localhost:3010/todolist/" + param.id)
      .then((response) => {
        const newList = response.data.data;
        setTask(newList.task);
        setMulai(newList.mulai);
        setAkhir(newList.akhir);
        setLaporan(newList.laporan);
      })
      .catch((eror) => {
        alert("terjadi kesalahan sir!" + eror);
      });
  }, []);
  const updateList = async (e) => {
    e.preventDefault();
    Swal.fire({
      title: "Apakah Ingin Di Update?",
      text: "Data kamu yang lama tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Di Update!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.put(
          "http://localhost:3010/todolist/" + param.id,{
            task:task,
            mulai:mulai,
            akhir:akhir,
            laporan:laporan,
            userId: localStorage.getItem("userId"),
        }
        );
        setTimeout(() => {
          window.location.reload();
       
          
        }, 1500);
        Swal.fire({
          icon: "success",
          title: "Berhasil mengedit data",
          showConfirmButton: false,
          timer: 1500,
        });
      }
      history.push("/todo");
    });
  };

  return (
    <div className='bg-gradient-to-r from-slate-300  to-cyan-500'>

      <div class="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
        <div class="mx-auto max-w-lg">


          <form onSubmit={updateList} action="" class="mt-6 mb-0 space-y-4 rounded-lg p-8 shadow-2xl">
            <h1 class="text-center text-2xl font-bold text-indigo-600 sm:text-3xl">
             EDIT
            </h1>

            <h1 class="mx-auto mt-4 max-w-md text-center text-gray-500">
              Mengubah List
            </h1>
            <div>
              <label for="email" class="text-sm font-medium">Task</label>

              <div class="relative mt-1">
                <input
                  type="text"
                  id="email"
                  class="w-full rounded-lg border-gray-200 p-2 pr-12 text-sm shadow-sm"
                  value={task}
                  onChange={(e) => setTask(e.target.value)}
                />


              </div>
            </div>

            <div>
              <label for="password" class="text-sm font-medium">Mulai</label>

              <div class="relative mt-1">
                <input
                  type="integer"
                  id="password"
                  class="w-full rounded-lg border-gray-200 p-2 pr-12 text-sm shadow-sm"
                  value={mulai}
                  onChange={(e) => setMulai(e.target.value)}
                />


              </div>
            </div>
            <div>
              <label for="password" class="text-sm font-medium">Akhir</label>

              <div class="relative mt-1">
                <input
                  type="text"
                  id="password"
                  class="w-full rounded-lg border-gray-200 p-2 pr-12 text-sm shadow-sm"
                  value={akhir}
                  onChange={(e) => setAkhir(e.target.value)}
                />


              </div>
            </div>
         

            <button
              type="submit"
              class="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white"
            >
              save
            </button>

            <p class="text-center text-sm text-gray-500">
              No account?
              <a class="underline" href="">Sign up</a>
            </p>
          </form>
        </div>
      </div>
    </div>
  )
}

