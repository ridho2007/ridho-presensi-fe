
import { useHistory, useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";
import axios from "axios";
import Swal from "sweetalert2";
export default function EditProfil() {
    const history = useHistory();
    const [nama, setNama] = useState("");
    const [nomor, setNomor] = useState();
    const [alamat, setAlamat] = useState();
    const [deskripsi, setDeskripsi] = useState();
   
    const [foto, setFoto] = useState([]);
    const param = useParams();
    useEffect(() => {
        axios
          .get("http://localhost:3010/profil/" + param.id)
          .then((response) => {
            const newList = response.data.data;
            setNama(newList.nama);
            setNomor(newList.nomor);
            setAlamat(newList.alamat);
            setDeskripsi(newList.deskripsi);
          })
          .catch((eror) => {
            alert("terjadi kesalahan sir!" + eror);
          });
      }, []);

      const updateList = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("file", foto)
        formData.append("nama", nama)
        formData.append("nomor", nomor)
        formData.append("alamat", alamat)
        formData.append("deskripsi", deskripsi)
        formData.append("userId", localStorage.getItem("userId"));
        Swal.fire({
          title: "Apakah ini di ganti?",
          showDenyButton: true,
          showCancelButton: false,
          confirmButtonText: "Ya",
          denyButtonText: "Tidak",
        }).then((result) => {
          if (result.isConfirmed) {
            axios
              .put(
               " http://localhost:3010/profil/" + param.id , formData ,
                //headers berikut berfunsi untuk method yang berada di akses admin
                {
                  headers: {
                    Authorization: `Bearer${localStorage.getItem("token")}`,
                    "Content-Type" : "multipart/form-data"
                  },
                }
              )
              .then(() => {
                history.push("/profil");
                Swal.fire("Berhasil!", "Data kamu di update", "success");
              })
              .catch((error) => {
                console.log(error);
              });
          }
        });
      };
  return (
    <div>
    <div className='bg-gradient-to-r from-slate-300  to-cyan-500'>

      <div class="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
        <div class="mx-auto max-w-lg">


          <form onSubmit={updateList} action="" class="mt-6 mb-0 space-y-4 rounded-lg p-8 shadow-2xl">
            <h1 class="text-center text-2xl font-bold text-indigo-600 sm:text-3xl">
             EDIT
            </h1>

            <h1 class="mx-auto mt-4 max-w-md text-center text-gray-500">
              Mengubah Profil
            </h1>
            <div>
              <label for="email" class="text-sm font-medium">Nama</label>

              <div class="relative mt-1">
                <input
                  type="text"
                  id="email"
                  class="w-full rounded-lg border-gray-200 p-2 pr-12 text-sm shadow-sm"
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                />


              </div>
            </div>

            <div>
              <label for="password" class="text-sm font-medium">Nomor hp</label>

              <div class="relative mt-1">
                <input
                  type="integer"
                  id="password"
                  class="w-full rounded-lg border-gray-200 p-2 pr-12 text-sm shadow-sm"
                  value={nomor}
                  onChange={(e) => setNomor(e.target.value)}
                />


              </div>
            </div>
            <div>
              <label for="password" class="text-sm font-medium">Alamat</label>

              <div class="relative mt-1">
                <input
                  type="text"
                  id="password"
                  class="w-full rounded-lg border-gray-200 p-2 pr-12 text-sm shadow-sm"
                  value={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                />


              </div>
            </div>
            <div>
              <label for="password" class="text-sm font-medium">Deskripsi</label>

              <div class="relative mt-1">
                <input
                  type="text"
                  id="password"
                  class="w-full rounded-lg border-gray-200 p-2 pr-12 text-sm shadow-sm"
                  value={deskripsi}
                  onChange={(e) => setDeskripsi(e.target.value)}
                />


              </div>
            </div>
            <div>
              <label for="password" class="text-sm font-medium">Deskripsi</label>

              <div class="relative mt-1">
                <input
                   type="file"
                  id="password"
                  class="w-full rounded-lg border-gray-200 p-2 pr-12 text-sm shadow-sm"
                 
                  onChange={(e) => setFoto(e.target.files[0])}
                />


              </div>
            </div>

            <button
              type="submit"
              class="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white"
            >
              save
            </button>

            <p class="text-center text-sm text-gray-500">
              No account?
              <a class="underline" href="">Sign up</a>
            </p>
          </form>
        </div>
      </div>
    </div></div>
  )
}
