import Navbar from "../Componen/NavigationBar";
import React, { useEffect, useState } from "react";
import { Modal, Form } from "react-bootstrap";
import { BsXSquareFill } from "react-icons/bs";
import { MdControlPoint } from "react-icons/md";
import axios from "axios";
import { FiEdit } from "react-icons/fi";
import Swal from "sweetalert2";
import { BsFillFileEarmarkPlusFill,BsClockFill } from "react-icons/bs";
export default function Todo() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [list, setList] = useState([]);
  const [akhir, setAkhir] = useState("");
  const [mulai, setMulai] = useState("");
  const [laporan, setLaporan] = useState("");
  const [task, setTask] = useState("");
  const [jam, setJam] = useState("");

  // addd
  const addTodoList = async (e) => {
    e.preventDefault();

    // Send the file and description to the server
    try {
      await axios.post(
        `http://localhost:3010/todolist?akhir=${akhir}&laporan=${laporan}&mulai=${mulai}&task=${task}&userId=${localStorage.getItem(
          "userId"
        )}`
      );
      setShow(false);
      Swal.fire("Sukses", "Berhasil Menambahkan!", "success");
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAll = (idx) => {
    axios
      .get(
        `http://localhost:3010/todolist?usersId=` +
          localStorage.getItem("userId")
      )
      .then((res) => {
        setList(res.data.data);
      })
      .catch(() => {
        // Swal.fire({
        //   icon: "error",
        //   title: "memunculkan data",
        //   showConfirmButton: false,
        //   timer: 1500,
        // });
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  const deleteTodolist = async (id) => {
    await Swal.fire({
      title: "Kamu Yakin Ingin Menghapus",
      text: "Jika Di hapus maka Tidak Akan Kembali",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, Hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3010/todolist/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      }
    });

    getAll(0);
  };
  return (
    <div className="bg-gradient-to-r from-slate-300  to-cyan-500 pb-[100%]  h-full">
      <div className=" w-full">
        <Navbar />
      </div>

      <div className="">
       
        <br />
        {/*  */}
        <div className="mx-[10%] ">
          <div onSubmit={addTodoList}>
          <label
            for="input-group-1"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
           Tugas
          </label>
          <div class="relative mb-6">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <BsFillFileEarmarkPlusFill />
            </div>
            <input
            onChange={(e) => setTask(e.target.value)}
              type="text"
              id="input-group-1"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Tugas"
            />
          </div>
          <label
            for="input-group-1"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
           Jam
          </label>
          <div class="relative mb-6">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <BsClockFill />
            </div>
            <input
               onChange={(e) => setJam(e.target.value)}
              type="time"
              id="input-group-1"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Tugas"
            />
          </div>

          <button type="submit" className="" >
          <a class=" flex rounded bg-indigo-600 px-2 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500">
            <MdControlPoint className="text-2xl" /> Add
          </a>
        </button>
        </div>
        </div>
       
       {/*  */}
       <br />

        <div class=" mx-[2%] grid grid-cols-4 gap-[1%]">
        {list.map((data, index) => {
                return (
  <div class="block p-6 rounded-lg shadow-lg bg-white max-w-sm">
    <h5 class="text-gray-900 text-xl leading-tight font-medium mb-2">{data.jam}</h5>
    <p class="text-gray-700 text-base mb-4">
      {data.task}
    </p>
    <div className="flex">
    <button type="button" class=" inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">Button</button>
    <p>{data.tanggal}</p>
    </div>
  </div>
  )})}
</div>

{/*  */}
        {/*  */}
        <br />
        <br />
        <h1 className="mx-[35%] mt-[10%] ">
          <h1 class=" bg-gradient-to-r from-green-500 via-blue-600 to-purple-700 bg-clip-text  font-extrabold text-transparent sm:text-5xl">
            List Harian
          </h1>
        </h1>

     
        <br />
        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>List yang Mau Di lakukan</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form >
              <Form.Group className="mb-3" controlId="formBasictext">
                <Form.Label>Task</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Apa Yang Mau Di Kerjakan"
                  value={task}
                  
                  required
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasictext">
                <Form.Label>Mulai</Form.Label>
                <Form.Control
                  type="time"
                  placeholder="Jam Berapa Mulai"
                  value={mulai}
                  onChange={(e) => setMulai(e.target.value)}
                  required
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicDate">
                <Form.Label>Akhir</Form.Label>
                <Form.Control
                  type="time"
                  placeholder="Jam Berapa Selesai"
                  value={akhir}
                  onChange={(e) => setAkhir(e.target.value)}
                  required
                />
              </Form.Group>

              <div className="flex gap-2">
                {/* jika di close maka akan keluar */}
                <button onClick={handleClose}>
                  <a class="inline-block rounded bg-red-600 px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500">
                    Close
                  </a>
                </button>
                <br />
                {/* jika di save maka akan menyimpan */}
                <button type="submit">
                  <a class="inline-block rounded bg-green-600 px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500">
                    Tambahkan
                  </a>
                </button>
              </div>
            </Form>
          </Modal.Body>
        </Modal>
        <br />
        {/* <div class=" mx-[10%]">
          <table class="min-w-full divide-y-2 divide-gray-200  dark:divide-gray-700 shadow-2xl">
            <thead>
              <tr className="bg-cyan-600">
                <th class=" text-center py-2  font-medium text-gray-900 dark:text-white">
                  No
                </th>
                <th class=" text-center py-2  font-medium text-gray-900 dark:text-white">
                  Task
                </th>
                <th class=" text-center py-2  font-medium text-gray-900 dark:text-white">
                  Mulai
                </th>
                <th class=" text-center py-2  font-medium text-gray-900 dark:text-white">
                  Akhir
                </th>
                <th class=" text-center py-2  font-medium text-gray-900 dark:text-white">
                  Laporan
                </th>

                <th class="text-center py-2"></th>
              </tr>
            </thead>

            <tbody class="divide-y divide-gray-200 dark:divide-gray-700">
              {list.map((data, index) => {
                return (
                  <tr className="border-1 bg-sky-400">
                    <td class=" text-center py-2 font-medium text-gray-900 dark:text-white">
                      {index + 1}
                    </td>
                    <td class=" text-center py-2 text-gray-700 dark:text-gray-200">
                      {data.task}
                    </td>
                    <td class=" text-center py-2 text-gray-700 dark:text-gray-200">
                      Jam {data.mulai}
                    </td>
                    <td class=" text-center py-2 text-gray-700 dark:text-gray-200">
                      Jam {data.akhir}
                    </td>
                    <td class=" text-center py-2">
                      {" "}
                      <label class="sr-only" for="Row1">
                        Row 1
                      </label>
                      <input
                        class="h-5 w-5 rounded border-gray-300"
                        type="checkbox"
                        id="Row1"
                      />
                    </td>

                    <td class=" text-center py-2 text-red-600 flex gap-2">
                      <button onClick={() => deleteTodolist(data.id)}>
                        <BsXSquareFill />
                      </button>

                      <a className="text-white" href={"/editlist/" + data.id}>
                        {" "}
                        <FiEdit />
                      </a>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div> */}
      </div>
    </div>
  );
}
