import React from 'react'
import "../Css/Home.css"
import Navbar from "../Componen/NavigationBar"
import todo from "../image/todolist.png"
function Home() {
  return (
    <div className='bg-gradient-to-r from-slate-300  to-cyan-500'>
        <Navbar/>
    <div >
      <br />
      <h1 className='text-white text-center text-3xl font-bold sm:text-4xl'>Jadwalkan <span className='text-indigo-200'> Absensi</span> & Perencanaan Tugas Dengan <span className='text-indigo-200'>Todo List</span></h1>
         <br />
         {/* Home 1 */}
        <div>
          
           
        <section>
  <div class="mx-auto max-w-screen-xl px-4 py-16 mt-[3%] sm:px-6 lg:px-8 shadow-2xl bg-sky-700 rounded-lg ">
    <div class="grid grid-cols-1 gap-8 lg:grid-cols-2 lg:gap-16">
      <div
        class="relative h-64 overflow-hidden rounded-lg sm:h-80 lg:order-last lg:h-full"
      >
        <img
          alt="Party"
          src="https://www.its.ac.id/mt/wp-content/uploads/sites/48/2020/01/presensi.png"
          class="absolute inset-0 h-full w-full object-cover"
        />
      </div>

      <div class="lg:py-24">
        <h2 class="text-3xl font-bold sm:text-4xl text-white">Kelola  <span className='text-indigo-300'> Presensi</span> Anda</h2>

        <p class="mt-4 text-white">
        Presensi Merupakan Aplikasi Absensi Berbasis Aplikasi. Dapatkan Laporan Absensi Dengan Mudah
        </p>

       <br />

       <a
  class="group relative inline-flex items-center overflow-hidden rounded bg-indigo-400 px-8 py-3 text-white focus:outline-none focus:ring active:bg-indigo-500"
  href="/jadwal"
>
  <span
    class="absolute right-0 translate-x-full transition-transform group-hover:-translate-x-4"
  >
    <svg
      class="h-5 w-5"
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
    >
      <path
        stroke-linecap="round"
        stroke-linejoin="round"
        stroke-width="2"
        d="M17 8l4 4m0 0l-4 4m4-4H3"
      />
    </svg>
  </span>

  <span class="text-sm font-medium transition-all group-hover:mr-4">
 Presensi
  </span>
</a>

      </div>
    </div>
  </div>
</section>
  {/* end */}
        </div>
        <br />
     {/* todolist */}
     <section>
  <div class="max-w-screen-xl px-4 py-8 mx-auto sm:py-12 sm:px-6 lg:px-8 shadow-2xl bg-sky-700">
    <div class="grid grid-cols-1 gap-4 lg:grid-cols-3 lg:items-stretch">
      <div class="grid p-6 bg-sky-700 rounded place-content-center sm:p-8">
        <div class="max-w-md mx-auto text-center lg:text-left">
          <header>
            <h2 class="text-xl font-bold text-white sm:text-3xl">Kelola To Do <span className='text-indigo-300'>List</span></h2>

            <p class="mt-4 text-white">
              To do List Berfungsi Sebagai Untuk Menjadwalkan Apa yang Akan Di Lakukan Hari Ini
            </p>
          </header>
<br />
          <a
  class="group relative inline-flex items-center overflow-hidden rounded bg-indigo-400 px-8 py-3 text-white focus:outline-none focus:ring active:bg-indigo-500"
  href="/todo"
>
  <span
    class="absolute right-0 translate-x-full transition-transform group-hover:-translate-x-4"
  >
    <svg
      class="h-5 w-5"
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
    >
      <path
        stroke-linecap="round"
        stroke-linejoin="round"
        stroke-width="2"
        d="M17 8l4 4m0 0l-4 4m4-4H3"
      />
    </svg>
  </span>

  <span class="text-sm font-medium transition-all group-hover:mr-4">
    To Do List
  </span>
</a>
        </div>
      </div>

      <div class="lg:col-span-2 lg:py-8">
        <ul class="grid grid-cols-2 gap-4">
          <li>
            <a href="#" class="block group">
              <img
                src="https://blog.stoplight.io/hubfs/API-To-Do-List.png#keepProtocol"
                alt=""
                class=" w-full rounded aspect-square"
              />

              <div class="mt-3">
          
              </div>
            </a>
          </li>

          <li>
            <a href="#" class="block group">
              <img
                src="https://res.cloudinary.com/dprnqhug7/image/upload/v1508226444/buat-todolist-app-vuejs_zdwhfi.gif"
                alt="todo"
                class=" w-full rounded aspect-square"
              />

              <div class="mt-3">
              
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>


     {/* end */}
      
        {/* home 2 */}
    <section class=" ">
  <div class="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8 ">
    <div class="mx-auto max-w-lg text-center">
      <h2 class="text-3xl font-bold sm:text-4xl text-gray-200">Mengapa <span className='text-indigo-700'> Presensi </span>
Lebih Baik?</h2>

      
    </div>

    <div class="mt-8 grid grid-cols-1 gap-8 md:grid-cols-2 lg:grid-cols-3">
      
{/* 1 */}
      <a
        class="bg-sky-700   block text-white hover:bg-sky-500 rounded-xl border border-indigo-600 p-8 shadow-xl transition hover:border-pink-500/10 hover:shadow-pink-500/10 hover:scale-105 sm:h-72 "
        href="/services/digital-campaigns"
      >
      <img src="https://presensi.co.id/assets/imgs/icons/minimalisir-kecurangan.svg" alt="" />

        <h2 class="mt-4 text-xl font-bold text-white ">Meminimalisir Kecurangan</h2>

        <p class="mt-1 text-sm  text-white ">
        Semua data absensi karyawan akan tersimpan secara digital sehingga meminimalisir kecurangan dan manipulasi data.
        </p>
      </a>
{/* 2 */}
      <a
        class="bg-sky-700  block text-white hover:bg-sky-500 rounded-xl border border-indigo-600 p-8 shadow-xl transition hover:border-pink-500/10 hover:shadow-pink-500/10 hover:scale-105 sm:h-72"
        href="/services/digital-campaigns"
      >
     <img src="https://presensi.co.id/assets/imgs/icons/praktis.svg" alt="" />

        <h2 class="mt-4 text-xl font-bold ">Lebih praktis</h2>

        <p class="mt-1 text-sm ">
        Dengan presensi tidak perlu antri lagi untuk absen, Anda dapat melakukan absensi melalui Smartphone masing-masing.
        </p>
      </a>
      {/* 3 */}
      <a
        class="bg-sky-700  block text-white hover:bg-sky-500 rounded-xl border border-indigo-600 p-8 shadow-xl transition hover:border-pink-500/10 hover:shadow-pink-500/10 hover:scale-105 sm:h-72"
        href="/services/digital-campaigns"
      >
        <img src="https://presensi.co.id/assets/imgs/icons/rekap-absensi.svg" alt="" />

        <h2 class="mt-4 text-xl font-bold ">Rekap Presensi Yang Baik</h2>

        <p class="mt-1 text-sm ">
        Presensi mencatat otomatis riwayat absensi Anda yang lengkap, Anda dapat melihat jumlah Hadir, Terlambat, Dll
        </p>
      </a>

    </div>

   
  </div>
</section>
{/* end 
 */}

    </div>
    </div>
  )
}

export default Home