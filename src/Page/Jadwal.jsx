
import Navbar from "../Componen/NavigationBar"
import { BsFillTrashFill } from "react-icons/bs";
import { GoPlus } from "react-icons/go";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { Modal, Form, Card } from "react-bootstrap";
import Swal from "sweetalert2";
import { AiFillDelete } from "react-icons/ai";
export default function Jadwal() {

  const [show, setShow] = useState(false);
  const [list, setList] = useState([]);
  const [pulang, setPulang] = useState([]);

  // const handleClose = () => setShow(false);
  // const handleShow = () => setShow(true);
  // const [nama, setNama] = useState("");
  // const [keterangan, setKeterangan] = useState("");

  const AbsenMasuk = async (e) => {
    e.preventDefault();

    // Send the file and description to the server
    try {
      await axios.post(`http://localhost:3010/presensi/absen_masuk?presensi=${"MASUK"}&userId=${localStorage.getItem("userId")}` , {
        usersId: localStorage.getItem("userId")
      });
      sessionStorage.setItem("masuk", "Sudah");
      setShow(false);
      Swal.fire("Good job!", "Kamu Berhasil Absen", "success");
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  const AbsenPulang = async (e) => {
    e.preventDefault();

    // Send the file and description to the server
    try {
      await axios.post(`http://localhost:3010/presensi/absen_pulang?presensi=${"PULANG"}&userId=${localStorage.getItem("userId")}` , {
        usersId: localStorage.getItem("userId")
      });
      // sessionStorage.setItem("masuk", "Sudah");
      setShow(false);
      Swal.fire("Good job!", "Kamu Berhasil Absen", "success");
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  // const getAll = async () => {
  //   await axios
  //     .get("http://localhost:3010/presensi?usersId=" +
  //       localStorage.getItem("userId"))
  //     .then((res) => {
  //       setMasuk(res.data.data);

  //     })
  //     .catch(() => {
  //       // Swal.fire({
  //       //   icon: "error",
  //       //   title: "memunculkan data",
  //       //   showConfirmButton: false,
  //       //   timer: 1500,
  //       // });
  //     });
  // };

  const getAll = async () => {
    await axios
      .get(
        "http://localhost:3010/presensi?usersId=" + localStorage.getItem("userId")
      )
      .then((res) => {
        setList(
          res.data.data.map((e, i) => ({
            ...e,
            no: i + 1,
          }))
        );
      })
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };

  const deletemasuk = async (id) => {
    await Swal.fire({
      title: "Kamu Yakin Ingin Menghapus",
      text: "Jika Di hapus maka Tidak Akan Kembali",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, Hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3010/presensi/" + id);
        Swal.fire("Deleted!", "Berhasil Menghapus", "success");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }
    });

    // getAll(0);
  };
 
  useEffect(() => {
    getAll();
   
  }, []);
console.log(list)
  return (

    <div className='bg-gradient-to-r from-slate-200  to-cyan-500 pb-[100%] h-[100%]'>

      <div className="fixed w-full">
        <Navbar />
      </div>

      <br />
      <br />
      <br />
      <h1
        class="text-center bg-gradient-to-r from-sky-700  to-purple-600 bg-clip-text text-3xl font-extrabold text-transparent sm:text-5xl"
      >
        Presensi


      </h1>
      <br />


      {sessionStorage.getItem("masuk") === null ? (
        <><button className="ml-[4%]" onClick={AbsenMasuk}>
          <span
            class="inline-block ml-[4%] mt-[] rounded hover:bg-indigo-400 bg-indigo-600 px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500"

          >Masuk
          </span>
        </button></>
      ) : (
        <><button className="ml-[4%]" onClick={AbsenPulang}>
        <span
          class="inline-block ml-[4%] mt-[] rounded hover:bg-indigo-400 bg-indigo-600 px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500"

        >pulang
        </span>
      </button></>
      )}

<div className="">
      <div className='mx-[4%] my-[4%] '>
        <div class="">
          <table class=" min-w-full divide-y divide-gray-200 text-sm  shadow-2xl">
            <thead class="bg-gray-500">
              <tr className="text-white">
                <th
                  class="whitespace-nowrap  text-center py-2  font-medium "
                >
                  Status
                </th>
                <th
                  class="whitespace-nowrap  text-center py-2  font-medium "
                >
                  Tanggal
                </th>
                <th
                  class="whitespace-nowrap  text-center py-2 font-medium "
                >
                  Absen
                </th>
                <th
                  class="whitespace-nowrap  text-center py-2 font-medium "
                >
                  Aksi
                </th>



              </tr>
            </thead>

            <tbody class="divide-y divide-gray-200 bg-slate-300">
              {list.map((lists) => {
                return (

                  <tr >
                    <td class="whitespace-nowrap text-center py-2 ">{lists.prsensi}
                    </td>
                    <td class="whitespace-nowrap text-center py-2 text-gray-700"> {lists.tanggal}</td>

                    <td class="whitespace-nowrap text-center py-2 ">Jam {lists.masuk}
                    </td>
                    <td class="whitespace-nowrap text-center text-red-600 py-2 ">
                      <button onClick={() => deletemasuk(lists.id)}><AiFillDelete/> </button>
                    </td>
                    <hr />
                  </tr>
                )
              })}


            </tbody>
          </table>
        </div>

      </div>
    


    
      </div>

    </div>
  )
}
