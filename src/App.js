import { BrowserRouter, Route, Switch } from 'react-router-dom';
import EditList from './Componen/EditList';
import EditProfil from './Componen/EditProfil';
import Login from './Componen/Login';

import NavigationBar from './Componen/NavigationBar';
import Profil from './Componen/Profil';
import Register from './Componen/Register';

import Home from './Page/Home';
import Jadwal from './Page/Jadwal';
import Todo from './Page/Todo';

// import './App.css';

function App() {
  return (
    <div className="App">
     <BrowserRouter>
     <main>
      <Switch>
      <Route path="/" component={Login} exact />
      <Route path="/register" component={Register} exact />
      <Route path="/home" component={Home} exact />
      <Route path="/navbar" component={NavigationBar} exact />
      <Route path="/jadwal" component={Jadwal} exact />
      <Route path="/todo" component={Todo} exact />
      <Route path="/profil" component={Profil} exact />
      <Route path="/editlist/:id" component={EditList} exact />
      <Route path="/editprofil/:id" component={EditProfil} exact />


      </Switch>
      </main></BrowserRouter>
    </div>
  );
}

export default App;
